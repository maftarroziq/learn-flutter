import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         home: Scaffold(
//             appBar: AppBar(title: Text('KepoinYuk')),
//             body: Column(children: [
//               Container(
//                   child: Center(
//                       child: Text(
//                     'Article: Eiusmod nulla ipsum irure sit eu sunt nulla mollit eiusmod Lorem commodo aliquip commodo sint. Aliquip ut culpa sit Lorem ut velit consequat sunt non labore eiusmod. Labore excepteur ex commodo nulla est aliqua dolor id eiusmod esse eu est enim proident. Fugiat excepteur sint cupidatat in amet minim duis culpa duis dolor consequat reprehenderit. Eiusmod qui esse sit eu exercitation.',
//                     style: TextStyle(
//                         color: Colors.white,
//                         fontSize: 19,
//                         fontStyle: FontStyle.italic,
//                         fontWeight: FontWeight.bold),
//                     maxLines: 5,
//                     overflow: TextOverflow.ellipsis,
//                     textAlign: TextAlign.center,
//                   )),
//                   color: Colors.lightBlueAccent,
//                   height: 200,
//                   width: 430),
//               Row(children: [
//                 Container(
//                     child: Center(
//                         child: Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Text(
//                           'About me\n',
//                           style: TextStyle(
//                               color: Colors.white,
//                               fontSize: 19,
//                               fontStyle: FontStyle.italic,
//                               fontWeight: FontWeight.bold),
//                           maxLines: 5,
//                           overflow: TextOverflow.ellipsis,
//                           // textAlign: TextAlign.center,
//                         ),
//                         Text(
//                           'Eiusmod ',
//                           style: TextStyle(
//                               color: Colors.white,
//                               fontSize: 19,
//                               fontStyle: FontStyle.italic,
//                               fontWeight: FontWeight.bold),
//                           maxLines: 5,
//                           overflow: TextOverflow.ellipsis,
//                           // textAlign: TextAlign.center,
//                         ),
//                       ],
//                     )),
//                     color: Colors.lightBlueAccent,
//                     height: 200,
//                     width: 210,
//                     margin: EdgeInsets.fromLTRB(0, 5, 5, 0),
//                     padding: EdgeInsets.all(10)),
//                 Container(
//                     child: Center(
//                         child: Text(
//                       'Event: Eiusmod nulla ipsum irure sit eu sunt nulla mollit eiusmod Lorem commodo aliquip commodo sint. Aliquip ut culpa sit Lorem ut velit consequat sunt non labore eiusmod. Labore excepteur ex commodo nulla est aliqua dolor id eiusmod esse eu est enim proident. Fugiat excepteur sint cupidatat in amet minim duis culpa duis dolor consequat reprehenderit. Eiusmod qui esse sit eu exercitation.',
//                       style: TextStyle(
//                           color: Colors.white,
//                           fontSize: 19,
//                           fontStyle: FontStyle.italic,
//                           fontWeight: FontWeight.bold),
//                       maxLines: 5,
//                       overflow: TextOverflow.ellipsis,
//                       textAlign: TextAlign.center,
//                     )),
//                     decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(20),
//                         gradient: LinearGradient(
//                             begin: Alignment.topLeft,
//                             end: Alignment.bottomRight,
//                             colors: [Colors.lightBlueAccent, Colors.blue])),
//                     // color: Colors.lightBlueAccent,
//                     height: 200,
//                     width: 210,
//                     margin: EdgeInsets.only(top: 5)),
//               ]),
//             ])));
//   }
// }

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int number = 0;

  void handleClickButton() {
    setState(() {
      number += 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('KepoinYuk Stateful'),
          ),
          body: ListApp()
          // body: Container(
          //   child: Center(
          //     child: Column(
          //       mainAxisAlignment: MainAxisAlignment.center,
          //       children: [
          //         Text(number.toString()),
          //         ElevatedButton(
          //             onPressed: handleClickButton, child: Text('Press me'))
          //       ],
          //     ),
          //   ),
          // ),
          ),
    );
  }
}

class ListApp extends StatefulWidget {
  @override
  _ListAppState createState() => _ListAppState();
}

class _ListAppState extends State<ListApp> {
  List<Widget> mylist = [];
  int counter = 1;

  // _ListAppState() {
  // for (int x = 1; x <= 25; x++)
  // mylist.add(Text(
  //   "Data ke-" + x.toString(),
  //   style: TextStyle(
  //     fontSize: 30,
  //   ),
  // ));
  // }

  void editList(type) {
    if (type == 'add') {
      mylist.add(Text("Data ke-" + counter.toString(),
          style: TextStyle(fontSize: 30)));
      setState(() {
        counter += 1;
      });
    } else {
      if (counter > 1) {
        mylist.removeLast();
        setState(() {
          counter -= 1;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                  onPressed: () => editList('add'), child: Text('Add')),
              ElevatedButton(
                  onPressed: () => editList(''), child: Text('Remove')),
            ],
          ),
          Column(
            children: mylist,
          )
        ],
      ),
    );
  }
}
